json.array!(@words) do |word|
  json.extract! word, :id, :original_word, :translation
  json.url word_url(word, format: :json)
end
